# Part 1/4

https://gitlab.com/ruslancer/supermetrics-hw-2/-/merge_requests/2/diffs

# Part 2/4

1. Hard coding credentials this way may cause problems in different environments.
Move sensitive data to the separate configuration/env file, these values shouldn't be present in the repository.

2. Function file_get_contents is limiting and error-prone. In order to be able to support a wider range of 
http operations it is better to use a library such as guzzlehttp (heavily maintained, rich functionality). 

3. In case of another request under the same API there will be a lot of duplication.
It is better to create a separate API class with base url and separate methods for requests, improve code reuse.

4. Add Dto classes for requests and responses. After the json string has been returned it must be unmarshalled,
validated (in case of an error - an exception needs to be thrown or an error returned), 
built into a Dto object.

5. Consider returning the token expiration time as a reference.

# Part 3/4

I prepared a set of classes. Note that they don't actually work since it was only 
built to illustrate how I would structure it. I didn't run it, I didn't add route processing, service provider, etc.

```
@see src folder
```

# Part 4/4

When working with modern frameworks/engines we deal with third party classes and libraries
all the time.
Using such solutions has its own advantages and disadvantages and should be considered thoroughly.
Software development is expensive. Therefore, programmers should always prioritize things which 
actually bring business value - new features, bug fixes, etc.
Why would you spend your time building something that has already been built in the past (tested,
debugged, optimized)? If you find a library, which can save you time, and you can be reasonably certain 
about the quality of this particular solution - use it.
Although, it is always better to decouple third party code from the business logic using wrapper classes, 
interfaces, etc. This way it will be easy to replace it in case the library becomes 
unreliable, abandoned or if you simply find a better solution.
