<?php

declare(strict_types=1);

namespace App\Model;

class User implements \JsonSerializable
{
    private int    $id;
    private string $email;
    private string $username;

    public function __construct(int $id, string $email, string $username)
    {
        $this->id       = $id;
        $this->email    = $email;
        $this->username = $username;
    }

    public static function createFromArray(array $data): self
    {
        return new self(
            $data['id'] ?? 0,
            $data['email'] ?? '',
            $data['username'] ?? ''
        );
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function jsonSerialize(): array
    {
        return [
            'id'       => $this->getId(),
            'email'    => $this->getEmail(),
            'username' => $this->getUsername(),
        ];
    }
}