<?php

declare(strict_types=1);

namespace App\Controller;

use App\DataProvider\UserDataProvider;
use App\Response\UsernameByEmailResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UsersController
{
    private UserDataProvider $userDataProvider;

    public function __construct(UserDataProvider $userDataProvider)
    {
        $this->userDataProvider = $userDataProvider;
    }

    public function usernameByEmail(Request $request): Response
    {
        $masterEmail = $request->get('email') ?? $request->get('masterEmail') ?? null;

        if (!$masterEmail) {
            return new Response('Unknown', Response::HTTP_BAD_REQUEST);
        }

        $user = $this->userDataProvider->getByEmail($masterEmail);
        if (!$user) {
            return new Response('Unknown', Response::HTTP_NOT_FOUND);
        }

        return new Response(
            (new UsernameByEmailResponse($user))->toString()
        );
    }


}