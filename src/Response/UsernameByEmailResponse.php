<?php
declare(strict_types=1);

namespace App\Response;

use App\Model\User;

class UsernameByEmailResponse
{
    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function toString(): string
    {
        return sprintf("The master email is %s\nUsername is %s", $this->user->getEmail(), $this->user->getUsername());
    }
}