<?php

declare(strict_types=1);

namespace App\DataProvider;

use App\Model\User;
use App\Repository\UserRepository;

class UserDataProvider
{
    const USER_BY_EMAIL_CACHE   = 'user_by_email_%s';
    const USER_BY_EMAIL_TIMEOUT = 3600 * 24;

    private UserRepository $userRepository;
    private \Redis         $redis;

    public function __construct(UserRepository $userRepository, \Redis $redis)
    {
        $this->userRepository = $userRepository;
        $this->redis          = $redis;
    }

    public function getByEmail(string $email, bool $useCache = true): ?User
    {
        $key = sprintf(self::USER_BY_EMAIL_CACHE, $email);

        $result = $this->redis->get($key);
        if ($useCache && $result) {
            return User::createFromArray(json_decode($result, true));
        }

        $user = $this->userRepository->getByEmail($email);

        $this->redis->set($key, json_encode($user), self::USER_BY_EMAIL_TIMEOUT);

        return $user;
    }
}