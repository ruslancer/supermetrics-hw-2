<?php

declare(strict_types=1);

namespace App\Repository;

use App\Model\User;

class UserRepository
{
    private \PDO $db;

    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    public function getByEmail(string $email): ?User
    {
        $stmt = $this->db->prepare('SELECT id, username, email FROM users WHERE email=? LIMIT 1');
        $stmt->execute([$email]);
        if ($row = $stmt->fetch()) {
            return User::createFromArray($row);
        }

        return null;
    }
}